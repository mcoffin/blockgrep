#[macro_use] extern crate clap;
extern crate regex;

mod util;

use clap::Clap;
use regex::Regex;
use std::{
    io::{
        self,
        BufRead,
        BufReader,
        Write,
    },
};
use util::RunnableConfig;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum State {
    Await,
    InBlock,
    Skip,
}

impl State {
    pub const fn new() -> State {
        State::Await
    }
}

#[derive(Debug, Clap)]
#[clap(about = crate_description!(), version = crate_version!(), author = crate_authors!())]
struct Config {
    #[clap(short = 'a', about = "Allow matching on all parts of a block, instead of just the first line")]
    match_anything: bool,
    #[clap(short = 'e', long, about = "Add a pattern to denote the end of a block, instead of checking for empty lines")]
    end_pattern: Option<Regex>,
    #[clap(long, about = "Replace end lines with this string instead")]
    separator: Option<String>,
    patterns: Vec<Regex>,
}

impl Config {
    #[inline(always)]
    pub fn separator<'a>(&'a self, line: &'a str) -> &'a str {
        self.separator.as_ref()
            .map(AsRef::as_ref)
            .unwrap_or(line)
    }

    #[inline(always)]
    pub fn patterns<'a>(&'a self) -> impl Iterator<Item=&'a Regex> + 'a {
        self.patterns.iter()
    }

    fn is_match<S: AsRef<str>>(&self, s: S) -> bool {
        let s = s.as_ref();
        self.patterns()
            .find(|&pat| pat.is_match(s))
            .is_some()
    }

    fn is_separator<S: AsRef<str>>(&self, s: S) -> bool {
        let s = s.as_ref();
        if let Some(end_pattern) = self.end_pattern.as_ref() {
            end_pattern.is_match(s)
        } else {
            s.len() < 1
        }
    }
}

impl RunnableConfig for Config {
    type Error = io::Error;

    fn run(&self) -> io::Result<()> {
        let mut state = State::new();
        let skip_state = if self.match_anything {
            State::Await
        } else {
            State::Skip
        };
        let stdin = BufReader::new(io::stdin());
        for line in stdin.lines() {
            let line = line?;
            match state {
                State::Await => if !self.is_separator(&line) {
                    if self.is_match(&line) {
                        state = State::InBlock;
                        println!("{}", &line);
                    } else {
                        state = skip_state;
                    }
                },
                State::InBlock if self.is_separator(&line) => {
                    println!("{}", self.separator(&line));
                    state = State::Await;
                },
                State::InBlock => {
                    println!("{}", &line);
                },
                State::Skip => if self.is_separator(&line) {
                    state = State::Await;
                },
            }
        }
        Ok(())
    }
}

fn main() {
    let config = Config::parse();
    if let Err(e) = config.run() {
        let mut stderr = io::stderr();
        let _r = writeln!(&mut stderr, "{}", &e);
    }
}
