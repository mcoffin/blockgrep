use std::error::Error;

pub trait RunnableConfig {
    type Error: Error;
    fn run(&self) -> Result<(), Self::Error>;
}
